<?php
  require_once dirname(__FILE__)."/components/templates/main.php";
  require_once dirname(__FILE__)."/koneksi.php";

  //Call Template
  $template = new Template();

  //Start HTML
    $template->pageTitle;

  //Start Content
    $template->contentTitle="<span class='glyphicon glyphicon-check'></span> Beranda";
    $template->mulaiContent();

?>

<!-- Content Here -->
<body class="signup-page">
    <div class="signup-box">
        <div class="card">
            <div class="header">
                <h2>
                    Form Peminjaman UMKM Mahasiswa
                </h2>
            </div>
            <div class="body">
            <form method="post" action="pinjaman.php">
            	<label for="email_address">Select ID Anggota</label>
				<div class="form-group">
                    <div class="form-line">
						<select class="form-control show-tick" name="anggota">
                            <option value="">-- Please select --</option>
							<?php
                                	$sql = "SELECT * FROM admin";
									$query = mysqli_query($connect,$sql) or die(mysqli_error($connect));
									while($result = mysqli_fetch_array($query)){									
							?>
                                	<option value="<?= $result['id'] ?>"><?= $result['nama'] ?></option>
                            <?php } ?>
						</select>
                    </div>
                </div>
            	
                <div class="msg">Register data diri dan jumlah pinjaman</div>
                <br>
                <label for="email_address">Masukkan NIM</label>
				<div class="form-group">
                    <div class="form-line">
						<input type="text" class="form-control" name="nim" placeholder="Masukan Nim" required autofocus>
                    </div>
                </div>
                
                <label for="email_address">Masukkan Nama</label>
				<div class="form-group">
                    <div class="form-line">
						<input type="nama" class="form-control" name="nama" placeholder="Nama" required>
                    </div>
                </div>
                
                <label for="email_address">Masukkan Jumlah Pinjaman</label>
				<div class="form-group">
                    <div class="form-line">
						<input type="text" class="form-control" name="jumlah" placeholder="Jumlah Pinjaman (Rp)" required>
                    </div>
                </div>
                
                <label for="email_address">Masukkan Waktu pinjaman (bln)</label>
				<div class="form-group">
                    <div class="form-line">
						<input type="date" class="form-control" name="waktu" placeholder="Jangka Waktu Peminjaman(Bln)" required>
                    </div>
                </div>
                
                
				<div class="form-group">
                    <div class="form-line">
						<button class="btn btn-block btn-lg bg-green waves-effect" type="submit">Finish</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>

<!-- Akhir Konten -->
<?php $template->endContent(); ?>

<!-- Place Script here -->

<!-- End </body> -->
<?php $template->akhirBody(); ?>

<!-- End HTML -->
<?php $template->akhirHTML(); ?>
