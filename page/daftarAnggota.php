<?php
  require_once dirname(__FILE__)."/../components/templates/main.php";
  require_once dirname(__FILE__)."/../koneksi.php";

  //Call Template
  $template = new Template();

  //Start HTML
    $template->pageTitle="Daftar Anggota";

  //Start Content
    $template->contentTitle="<span class='glyphicon glyphicon-check'></span> Beranda";
    $template->mulaiContent();

?>

<!-- Akhir Konten -->
<?php $template->endContent(); ?>
<!-- End </body> -->
<?php $template->akhirBody(); ?>

<!-- End HTML -->
<?php $template->akhirHTML(); ?>
