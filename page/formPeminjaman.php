<?php
  require_once dirname(__FILE__)."/../components/templates/main.php";
  require_once dirname(__FILE__)."/../koneksi.php";

  //Call Template
  $template = new Template();

  //Start HTML
    $template->pageTitle="Form Peminjaman";

  //Start Content
    $template->contentTitle="<span class='glyphicon glyphicon-check'></span> Form Peminjaman";
    $template->mulaiContent();
?>


<body class="signup-page">
    <div class="signup-box">
        <div class="card">
            <div class="header">
                <h2>
                    Form Peminjaman UMKM Mahasiswa
                </h2>
            </div>
            <div class="body">
                <div class="row clearfix">
                    <form id="sign_up" method="POST">
                        <div class="col-sm-12">
                            <label>Register data diri dan jumlah pinjaman</label>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">person</i>
                                    </span>
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="id" placeholder="Id" required autofocus>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">perm_phone_msg</i>
                                    </span>
                                    <div class="form-line">
                                        <input type="email" class="form-control" name="Hp" placeholder="Phone Number" required>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">place</i>
                                    </span>
                                    <div class="form-line">
                                        <input type="password" class="form-control" name="alamat" minlength="6" placeholder="Addres" required>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">payment</i>
                                    </span>
                                    <div class="form-line">
                                        <input type="password" class="form-control" name="Jumlah" minlength="6" placeholder="Jumlah Pinjaman (Rp)" required>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">date_range</i>
                                    </span>
                                    <div class="form-line">
                                        <input type="text" class="datepicker form-control" placeholder="Please choose a date...">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <button class="btn btn-block btn-lg bg-green waves-effect" type="submit">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

 <!-- Akhir Konten -->
<?php $template->endContent(); ?>

<!-- Place Script here -->

<!-- End </body> -->
<?php $template->akhirBody(); ?>

<!-- End HTML -->
<?php $template->akhirHTML(); ?>