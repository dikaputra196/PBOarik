			</div>
        </div>
    </section>

<!-- Jquery Core Js -->
<script src="<?= MAIN_URL ?>/plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="<?= MAIN_URL ?>/plugins/bootstrap/js/bootstrap.js"></script>

<!-- Select Plugin Js -->
<script src="<?= MAIN_URL ?>/plugins/bootstrap-select/js/bootstrap-select.js"></script>

<!-- Slimscroll Plugin Js -->
<script src="<?= MAIN_URL ?>/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="<?= MAIN_URL ?>/plugins/node-waves/waves.js"></script>

<!-- Autosize Plugin Js -->
<script src="<?= MAIN_URL ?>/plugins/autosize/autosize.js"></script>

<!-- Moment Plugin Js -->
<script src="<?= MAIN_URL ?>/plugins/momentjs/moment.js"></script>

<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="<?= MAIN_URL ?>/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

<!-- Custom Js -->
<script src="<?= MAIN_URL ?>/js/admin.js"></script>
<script src="<?= MAIN_URL ?>/js/pages/forms/basic-form-elements.js"></script>

<!-- Demo Js -->
<script src="<?= MAIN_URL ?>/js/demo.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>