<?php
  require_once dirname(__FILE__)."/../../koneksi.php";

  class template{
    public $pageTitle = "Project PBO";

    public function mulaiContent(){
		 session_start();
          if(!isset($_SESSION['id'])){
        		echo "<script>
        			alert('Silahkan Login Kembali');location.href='".MAIN_URL."/index.php';
        		</script>";
        	}
        $this->startHtml();
        echo $this->templateHeader();
        echo $this->templateSidebar();
        echo $this->templateContent();
    }

    //End Content
        public function endContent(){
            echo $this->footerTemplate();
        }

    //Start HTML
        public function startHtml(){
          echo "
          <!DOCTYPE html>
          <html>
          <head>
              <meta charset='UTF-8'>
              <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
              <title></title>
              <!-- Favicon-->
              <link rel='icon' href='#' type='image/x-icon'>

              <!-- Google Fonts -->
              <link href='https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
              <link href='https://fonts.googleapis.com/icon?family=Material+Icons' rel='stylesheet' type='text/css'>

              <!-- Bootstrap Core Css -->
              <link href='".MAIN_URL."/plugins/bootstrap/css/bootstrap.css' rel='stylesheet'>

              <!-- Waves Effect Css -->
              <link href='".MAIN_URL."/plugins/node-waves/waves.css' rel='stylesheet' />

              <!-- Animation Css -->
              <link href='".MAIN_URL."/plugins/animate-css/animate.css' rel='stylesheet' />

              <!-- Bootstrap Material Datetime Picker Css -->
              <link href='".MAIN_URL."/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css' rel='stylesheet' />

              <!-- Wait Me Css -->
              <link href='".MAIN_URL."/plugins/waitme/waitMe.css' rel='stylesheet' />

              <!-- Bootstrap Select Css -->
              <link href='".MAIN_URL."/plugins/bootstrap-select/css/bootstrap-select.css' rel='stylesheet' />

              <!-- Custom Css -->
              <link href='".MAIN_URL."/css/style.css' rel='stylesheet'>

              <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
              <link href='".MAIN_URL."/css/themes/all-themes.css' rel='stylesheet' />
          </head>
          ";
  }

  public function akhirBody(){
    echo "</body>";
  }

  public function akhirHTML(){
    echo "</html>";
  }

  private function templateContent(){
            ob_start();
            include "content.php";
            $val = ob_get_contents();
            ob_end_clean();

            return $val;
        }

        private function templateHeader() {
            ob_start();
            include "header.php";
            $header = ob_get_contents();
            ob_end_clean();

            return $header;
        }

        private function footerTemplate(){
            ob_start();
            include "footer.php";
            $val = ob_get_contents();
            ob_end_clean();

            return $val;
        }

        private function templateSidebar(){
            ob_start();
            include "sidebar.php";
            $val = ob_get_contents();
            ob_end_clean();

            return $val;
        }
  }
?>
