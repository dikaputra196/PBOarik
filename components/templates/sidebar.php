<section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?= $_SESSION['nama']; ?></div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="<?= MAIN_URL ?>/logout.php"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header active">MAIN NAVIGATION</li>
                    <li>
                    <a href="<?= MAIN_URL ?>/index.php">
                        <i class="material-icons">home</i>
                        <span>Home</span>
                    </a>
                    </li>
                    <li>
                        <a href="<?= MAIN_URL ?>/viewanggota.php">
                            <i class="material-icons">person</i>
                            <span>Daftar Anggota</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?=MAIN_URL?>/formpeminjaman.php">
                            <i class="material-icons">toc</i>
                            <span>Pinjaman</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="material-icons">payment</i>
                            <span>Pengembalian</span>
                        </a>
                    </li>
                   
                    
                    
                    
                    
                    
                    
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                </div>
                <div class="version">
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
</section>