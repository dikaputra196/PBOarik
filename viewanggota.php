<?php
  require_once dirname(__FILE__)."/components/templates/main.php";
  require_once dirname(__FILE__)."/koneksi.php";

  //Call Template
  $template = new Template();

  //Start HTML
    $template->pageTitle;

  //Start Content
    $template->contentTitle="<span class='glyphicon glyphicon-check'></span> Beranda";
    $template->mulaiContent();

?>

<!-- Content Here -->
<body class="signup-page">
    <div class="signup-box">
        <div class="card">
            <div class="header">
                <h1>
                    DAFTAR ANGGOTA
                </h1>
            </div>
            <div class="body">
            <table class="table">
            	<thead>
                	<th>ID</th>
                    <th>Nama</th>
                    <th>No Hp</th>
                    <th>Status</th>
                    <th>Action</th>
                </thead>
                
                <?php
				
				
                	$x=mysqli_query($connect,"SELECT * FROM admin where status='1' ");
					
					while($a=mysqli_fetch_array($x)){?>
						<tr>
                        	<td><?= $a['id'] ?></td>
                            <td><?= $a['nama'] ?></td>
                            <td><?= $a['nomer_hp'] ?></td>
                            <td><?= $a['status'] ?></td>
                			<td>
                            	 <a class="delete-item" href="javascript:void(0)" data-id="<?= $a["id"]; ?>"><button class="btn btn-sm btn-danger"> Delete</button></a>
                            </td>            
                        </tr>
						<?php }
					?>
                <tbody>
                </tbody>
            </table>
            </div>
        </div>
    </div>

<!-- Akhir Konten -->
<?php $template->endContent(); ?>

<!-- Place Script here -->
 <script>
    $(document).ready(function(){
      $(".delete-item").click(function(e){
          e.preventDefault();
          var rowid = $(this).attr('data-id');
          var parent = $(this).parent("td").parent("tr");
          bootbox.dialog({
            message: "Apakah anda yakin akan menghapus Data ini?",
            title: "<i class='glyphicon glyphicon-trash'></i> Hapus? ",
            buttons: {
            success: {
            label: "<i class='fa fa-times'></i> Tidak",
            className: "btn-success",
            callback: function(){
              $(".bootbox").modal("hide");
            }
          },
          danger: {
            label: "<i class='fa fa-check'></i> Hapus!",
            className: "btn-danger",
            callback: function(){
              $.ajax({
                type: "POST",
                url: "<?= MAIN_URL ?>/deleteAnggota.php",
                data: "rowid="+rowid
              })
              .done(function(response){
                bootbox.alert(response);
                parent.fadeOut('slow');
              })
              .fail(function(){
                bootbox.alert('Error.....');
              });
            }
          }
          }
          });
      });
    });
  </script>



<!-- End </body> -->
<?php $template->akhirBody(); ?>

<!-- End HTML -->
<?php $template->akhirHTML(); ?>
